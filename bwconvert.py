import numpy as np
from PIL import Image, ImageFilter, ImageOps
import os
from progressbar import Bar, Counter, AdaptiveETA, ProgressBar, Percentage
widgets = [Counter(), ' ',  Bar(), ' ', AdaptiveETA(), ' ', Percentage()]

def normalize_image(img, fill = np.array([255, 255, 255], dtype='uint8')):
    img_arr = np.array(img)
    pixels = img_arr.reshape((-1, 3))
    colors, counts = np.unique(pixels, return_counts=True, axis=0)
    mode = colors[np.argsort(counts)[::-1]][0]
    normed = pixels - mode + fill
    normed = normed.reshape(img_arr.shape)
    return Image.fromarray(normed)

def filter_quantize(pil_img, save=True):
    gray = ImageOps.grayscale(pil_img)
    smoothed = gray.filter(ImageFilter.MedianFilter).filter(ImageFilter.MinFilter)
    two_color = smoothed.quantize(2).convert(mode='RGB')
    contrast_adjusted = ImageOps.autocontrast(two_color)
    result = normalize_image(two_color)
    contrast_adjusted_result = ImageOps.autocontrast(result)
    return contrast_adjusted_result

def load_and_process_image(image_path):
    rgb_im = Image.open(image_path)
    return rgb_im

source1 = '/home/rahul/mason/cdcfr/samples/testclassifier'
for root, dirs, files in os.walk(source1):
                num_imgs = len(files)
                file_iter = iter(files)
                break
pg_bar = ProgressBar(maxval=num_imgs,
                    widgets=widgets,
                    term_width=80)
pg_bar.start()
write_index = 0
for root, dirs, files in os.walk(source1, topdown=False):
    for name in files:
        try:
            if(len(name.split('.')) == 2):
                newFileName = name.split('.')[0] + '_bw' + '.' + name.split('.')[1]
            else:
                newFileName = name
            oldfileNames = os.path.join('/home/rahul/mason/cdcfr/Convert_bw/', newFileName)
            if os.path.isfile(oldfileNames) == True:
                continue
            filePath = os.path.join(root, name)
            img = filter_quantize(load_and_process_image(filePath))
            img.save(os.path.join('/home/rahul/mason/cdcfr/Convert_bw/', newFileName))
            write_index += 1
            pg_bar.update(write_index)
        except Exception:
            continue
