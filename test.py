from keras.models import load_model
from keras.preprocessing.image import load_img, img_to_array
import numpy as np

img_width, img_height = 224, 224

im = input('select image to test: ')

im = load_img(im, target_size=(img_width,img_height))
im = img_to_array(im)
im = np.expand_dims(im, axis=0)
im/=255

model = load_model('/home/rahul/mason/cdcfr/models/simp_CNN_outlinedetect.h5')

print(model.predict(im))
