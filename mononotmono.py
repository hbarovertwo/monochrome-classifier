from keras.layers import Conv2D, MaxPooling2D, ZeroPadding2D
from keras.layers import Dense, Activation, Dropout, Flatten
from keras import optimizers
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator
import numpy as np 
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

# step 1: load data

img_width = 224
img_height = 224
train_data_dir = 'bg/train'
valid_data_dir = 'bg/validate'

train_datagen = ImageDataGenerator(
        rescale = 1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1./255,horizontal_flip=True)

train_generator = train_datagen.flow_from_directory(directory=train_data_dir,
											   target_size=(img_width,img_height),
											   classes=['bw','white'],
											   class_mode='binary',
											   batch_size=50)

validation_generator = test_datagen.flow_from_directory(directory=valid_data_dir,
											   target_size=(img_width,img_height),
											   classes=['bw','white'],
											   class_mode='binary',
											   batch_size=50)


# step-2 : build model

model =Sequential()

model.add(Conv2D(32,(3,3), input_shape=(img_width, img_height, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(32,(3,3), input_shape=(img_width, img_height, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(64,(3,3), input_shape=(img_width, img_height, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Flatten())
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])

print('model compiled')

print('starting training...')
training = model.fit_generator(generator=train_generator, steps_per_epoch= 6000 // 50,epochs=15,validation_data=validation_generator,validation_steps=2200//50)

print('training finished')

print('saving model to simp_CNN_bwwhite.h5')

model.save('models/simp_CNN_bwwhite.h5')
